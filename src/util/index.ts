import vscode, { TextDocument, workspace, window } from "vscode";
import path from 'path';
import fs from 'fs';
import config from "../config";

function showError(str: string) {
  throw str;
}

/**
 * 项目根目录
 */
let _projectPath: string;

/**
 * 获取项目路径
 * @param document 
 */
export function getProjectPath(this: any, document: TextDocument) {
  if (_projectPath) return _projectPath;
  if (!document && window.activeTextEditor) {
    document = window.activeTextEditor.document;
  }
  if (!document) {
    showError('当前激活的编辑器不是文件或者没有文件被打开！');
  }

  const workspaceFolder = workspace.getWorkspaceFolder(document.uri);

  console.log("workspaceFolder", workspaceFolder);
  if (!workspaceFolder) {
    showError('获取工程根路径异常！');
    return '';
  }
  _projectPath = workspaceFolder.uri.fsPath;
  return _projectPath;
}

let _basePath = "";
/**
 * 
 * @param document 
 */
export function getBasePath(document: TextDocument) {
  if (!_basePath) {
    _basePath = path.join(getProjectPath(document), config.baseDir);
  }
  return _basePath;
}