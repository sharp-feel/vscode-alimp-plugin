import * as vscode from "vscode";
import registerTagLocation from "./location/tag";
import registerLanguageAxml from "./languages/axml";
// 插件激活时的入口
export function activate(context: vscode.ExtensionContext) {
  registerLanguageAxml();
  registerTagLocation(context);
}

// 插件释放的时候触发
export function deactivate() {}