"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var tag_1 = __importDefault(require("./location/tag"));
var axml_1 = __importDefault(require("./languages/axml"));
// 插件激活时的入口
function activate(context) {
    axml_1.default();
    tag_1.default(context);
}
exports.activate = activate;
// 插件释放的时候触发
function deactivate() { }
exports.deactivate = deactivate;
