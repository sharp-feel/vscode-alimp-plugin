"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var vscode_1 = require("vscode");
var path_1 = __importDefault(require("path"));
var config_1 = __importDefault(require("../config"));
function showError(str) {
    throw str;
}
/**
 * 项目根目录
 */
var _projectPath;
/**
 * 获取项目路径
 * @param document
 */
function getProjectPath(document) {
    if (_projectPath)
        return _projectPath;
    if (!document && vscode_1.window.activeTextEditor) {
        document = vscode_1.window.activeTextEditor.document;
    }
    if (!document) {
        showError('当前激活的编辑器不是文件或者没有文件被打开！');
    }
    var workspaceFolder = vscode_1.workspace.getWorkspaceFolder(document.uri);
    console.log("workspaceFolder", workspaceFolder);
    if (!workspaceFolder) {
        showError('获取工程根路径异常！');
        return '';
    }
    _projectPath = workspaceFolder.uri.fsPath;
    return _projectPath;
}
exports.getProjectPath = getProjectPath;
var _basePath = "";
/**
 *
 * @param document
 */
function getBasePath(document) {
    if (!_basePath) {
        _basePath = path_1.default.join(getProjectPath(document), config_1.default.baseDir);
    }
    return _basePath;
}
exports.getBasePath = getBasePath;
