"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 跳转到定义示例，本示例支持package.json中dependencies、devDependencies跳转到对应依赖包。
 */
var vscode_1 = __importDefault(require("vscode"));
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var index_1 = require("../util/index");
var config_1 = __importDefault(require("../config"));
function matchTagFile(projectPath, _path) {
    var destPath = path_1.default.join(projectPath, _path);
    if (!destPath.endsWith("." + config_1.default.xmlSuffix)) {
        destPath += "." + config_1.default.xmlSuffix;
    }
    console.log("[tag]", destPath);
    if (fs_1.default.existsSync(destPath)) {
        return new vscode_1.default.Location(vscode_1.default.Uri.file(destPath), new vscode_1.default.Position(0, 0));
    }
}
/**
 * 查找文件定义的provider，匹配到了就return一个location，否则不做处理
 */
function provideDefinition(document, position, token) {
    var fileName = document.fileName;
    var line = document.lineAt(position);
    var projectPath = index_1.getBasePath(document);
    // 只处理package.json文件
    if (fileName.endsWith(".json")) {
        try {
            var json = JSON.parse(document.getText());
            if (json.usingComponents) {
                var match = line.text.match(/"([^.]+)"\s*:\s*"([^.]+)"/);
                if (match && match[1] && json.usingComponents[match[1]] && match[2]) {
                    return matchTagFile(projectPath, match[2]);
                }
            }
        }
        catch (error) {
            console.log("[tag]", error);
        }
    }
    else if (fileName.endsWith(config_1.default.xmlSuffix)) {
        var workDir = path_1.default.dirname(fileName);
        var word = document.getText(document.getWordRangeAtPosition(position, /[\w-]+/g));
        var jsonFile = path_1.default.join(workDir, path_1.default.basename(fileName, "." + config_1.default.xmlSuffix) + ".json");
        if (fs_1.default.existsSync(jsonFile)) {
            var code = fs_1.default.readFileSync(jsonFile, 'utf-8');
            try {
                var json = JSON.parse(code);
                console.log(json, json.usingComponents, word);
                if (json.usingComponents && json.usingComponents[word]) {
                    return matchTagFile(projectPath, json.usingComponents[word]);
                }
            }
            catch (error) {
                console.log("[tag]", error);
            }
        }
    }
}
/**
 * 注册标签定位
 * @param context
 */
function registerTagLocation(context) {
    // 注册如何实现跳转到定义
    context.subscriptions.push(vscode_1.default.languages.registerDefinitionProvider(['json', config_1.default.xmlSuffix], {
        provideDefinition: provideDefinition
    }));
}
exports.default = registerTagLocation;
